<?php

namespace App\Constant;

class AppConstant
{
    const APP = 'cms';
    const APP_NAME = 'ModStartCMS';
    const VERSION = '7.5.0';
}
